# Seven style guide

Drupal interface standards have been established in Drupal and apply to user interfaces within Drupal and its
contributed modules. All new interfaces should follow the current standards, and document any new additions or changes.

This library will cover the most important interface elements and provide recommendations based on usage in contributed
modules and best practices that follow from design and user research activities.
